﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Linq;
using System.Security.AccessControl;

namespace DacalRipper
{
    class Program
    {
        private const int TOTAL_DISKS = 100;

        static void Main(string[] args)
        {
            var source = @"H:";
            
            var destin = @"C:";

            try
            {
                var dacalRipper = new DacalRipper();
                dacalRipper.Init(0);

                int currentlyInPos;
                currentlyInPos = dacalRipper.USBCDGetDriveCD();
                if (currentlyInPos > 0)
                {
                    //dacalRipper.USBCDMovetoDrive(currentlyInPos);
                    //dacalRipper.Eject();
                }
                    
                for (int i = 1; i<= TOTAL_DISKS; i++) {
                    Console.Write(string.Format("CD {0}", i));

                    dacalRipper.USBCDMovetoDrive(i);
                    
                    Console.WriteLine(string.Format(" -> {0}", GetDriveName(source)));

                    var drives = DriveInfo.GetDrives();
                    var sourceDrive = drives.First(d => d.Name.StartsWith(source));
                    //var destDrive = drives.First(d => d.Name.StartsWith(destin)); //doesnt work with network drives
                    var destDrive = new DriveInfo(destin);

                    Wait(sourceDrive);
                    //Wait(destDrive); //doesnt work with network drives

                    //var destDir = destDrive.RootDirectory.CreateSubdirectory(GetName(source));
                    var destPth = GetDestPath(destDrive, GetName(source));
                    var destDir = Directory.CreateDirectory(destPth);
                    Directory.CreateDirectory(destDir.FullName);

                    CopyFilesRecursively(source, destDir.FullName);
                    
                    dacalRipper.Eject();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            Console.WriteLine("ok");
            Console.ReadKey();
        }

        private static string GetDestPath(DriveInfo destDrive, string source)
        {
            var path = Path.Combine(destDrive.RootDirectory.Name, source);
            if (Directory.Exists(path))
                path = Path.Combine(destDrive.RootDirectory.Name, RandomString(10));
            return path;
        }

        private static void Wait(DriveInfo drive)
        {
            while (!drive.IsReady)
            {
                Console.Write(".");
                Thread.Sleep(1000);
            }
        }

        private static void CopyFilesRecursively(String SourcePath, String DestinationPath)
        {
            Thread.Sleep(1500);

            // First create all of the directories
            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
            {
                var dirPath2 = dirPath.Replace(SourcePath, DestinationPath);
                //Console.WriteLine("creating {0}", dirPath2);
                Directory.CreateDirectory(dirPath2);
            }

            // Copy all the files
            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
            {
                var newPath2 = Path.Combine(DestinationPath, newPath.Replace(SourcePath, ""));
                //Console.WriteLine("copying {0}", newPath2);
                Console.Write(".");
                File.Copy(newPath, newPath2, true);
            }
            Console.WriteLine();
            Thread.Sleep(3300);
        }

        private static string GetName(string SourcePath)
        {
            if (GetDriveName(SourcePath) != null)
                return new DriveInfo(SourcePath).VolumeLabel;

            foreach (string dirPath in Directory.GetDirectories(SourcePath, "*", SearchOption.AllDirectories))
                return dirPath;

            foreach (string newPath in Directory.GetFiles(SourcePath, "*.*", SearchOption.AllDirectories))
                return newPath;

            return RandomString(10);
        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static string GetDriveName(string SourcePath)
        {
            if (new DriveInfo(SourcePath).VolumeLabel != null)
                return new DriveInfo(SourcePath).VolumeLabel;
            return null;
        }



        class DacalRipper
        {
            private int deviceId = 0;

            public DacalRipper()
            {
                DacalRipperAPI.InitUSBCDLibrary();
            }
        
            ~DacalRipper () {
                DacalRipperAPI.CloseUSBCDLibrary();
            }


            public void Init(int pos = 0)
            {
                var totalDevices = DacalRipperAPI.GetDeviceNumber();
                if (totalDevices > 0)
                {
                    deviceId = DacalRipperAPI.EnumDevice(pos);
                    /*
                    string type = GetDeviceType();
                    if (type != "DC016RW")
                        throw new Exception(string.Format("Dacal {0} does not seem to be a PowerDrive", type));       
                        */ 
                }
                if (deviceId == 0)
                    throw new Exception("no Dacal PowerDrive device connected");
            }

            private string GetDeviceType()
            {
                int size = 512;
                string type = new string('*', 512);
                Wait(DacalRipperAPI.USBCDGetType(deviceId, type, ref size));
                return type.Substring(0, size);
            }

            public int USBCDGetDriveCD()
            {
                return DacalRipperAPI.USBCDGetDriveCD(deviceId);
            }

            public DacalRipperAPIState USBCDMovetoDrive(int i)
            {
                return Wait(DacalRipperAPI.USBCDMovetoDrive(deviceId, i), 10);
            }

            public DacalRipperAPIState Eject()
            {
                return Wait(DacalRipperAPI.USBCDEject(deviceId), 10);
            }

            internal DacalRipperAPIState USBCDMoveto(int currentlyInPos)
            {
                return Wait(DacalRipperAPI.USBCDMoveto(deviceId, currentlyInPos), 10);
            }

          

            internal DacalRipperAPIState Wait(int commandRes, int waitForMoreSeconds = 0)
            {
                while (DacalRipperAPI.USBCDGetStatus(deviceId) != DacalRipperAPI.gDEVICE_COMMANDOK)
                {
                    Console.Write(".");
                    Thread.Sleep(1000);
                }
                Console.WriteLine();

                if (waitForMoreSeconds > 0)
                {
                    Thread.Sleep(TimeSpan.FromSeconds(0));
                }

                return (DacalRipperAPIState) DacalRipperAPI.USBCDGetStatus(deviceId);
            }

        }

        enum DacalRipperAPIState
        {
            OK = 0,
            DeviceIdError = 1,
            Busy = 2,
            UnknownError = 3,
        }

        class DacalRipperAPI
        {
            public const int gDEVICE_COMMANDOK = 0;
            public const int gDEVICE_IDERROR = 1;
            public const int gDEVICE_BUSY = 2;
            public const int gDEVICE_UNKNOWNERROR = 3;


            // Functions to init USB CD Library
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern void InitUSBCDLibrary();
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern void CloseUSBCDLibrary();
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDeviceNumber();
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int EnumDevice(int gIndex);

            // Functions to operate USB CD Library

            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDReset(int gID);
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDMoveto(int gID, int gIndex);
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDGetCDDown(int gID);
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDLEDON(int gID);
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDLEDOFF(int gID);
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDGetStatus(int gID);


            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDGetType(int gID, string str, ref int sLen);


            //command returns the slot location of the disc that is currently in the drive.
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDGetDriveCD(int gID);

            //moves the disc in the specified location into the drive
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDMovetoDrive(int gID, int gIndex);

            //ejects the cd from the drive
            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDEject(int gID);


            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int GetDriveCounter(int gID, int driveCounter);

            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int ClearDriveCounter(int gID);

            [DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            public static extern int USBCDchanged();


            //[DllImport("sucdapi.dll", CallingConvention = CallingConvention.Winapi)]
            //public static extern int USBCDSetDriveDisc(int gID, int gIndex);

            /*
            
            USBCDDLL library Usage Help:

1. When start use this library, you must call InitUSBCDLibrary(), and when you no int need use this library, you should call   
      CloseUSBCDLibrary(), Usually, call InitUSBCDLibrary() when your application start and call CloseUSCCDLibrary() when your
      Application terminated.

2. After this library is initialize, the next thing is to get the number of device on your system, because USB can support up to 127 devices
    on each system, you can call GetDeviceNumber(), which will return the device number now exist on your system.

3. After you know how many device exist on your system, you must also know each device's ID, each of our decvice has a fixed ID,
    so you can control different device via device ID. To get device ID, call  EnumDevice( int iIndex ), which pass the Index of device
    you want to get, from 0. below is a code sample to get all device' device ID on your system.
   
    int TotalDeviceNumber = GetDeviceNumber();
    for ( int i =0;i<TotalDeviceNumber;i++)
    {
      int DeviceID = EnumDevice( i );
      //.............
      // you should save DeviceID for later used.
     }

4. After you get device ID list, now you can control this use device via DeviceID. below is functions which our device support.

   int USBCDReset( int ID )
     Reset Device.

   int USBCDMoveto( int ID, int Index )
     Move device to the position( Index ) of CD and GetCD.

   int USBCDGetCDDown( int ID )
     Get now position's CD Down.

   int USBCDLEDON( int ID )
     Set LED on device On, LED on device is for show now CD's positoin.

   int USBCDLEDOFF( int ID )
     Set LED on Device Off.

   int USBCDGetStatus( int ID )
     Get device's now status.

Each function will return a status code : 
   DEVICE_COMMANDOK : Command success.
   DEVICE_IDERROR : Device ID not exist.
   DEVICE_BUSY : Device is now busy, please try later.
   DEVICE_UNKNOWERROR : unknow error.

Because some function must spend some time to finish ( ex : move cd to a int distance, GetCDDown ), so
 when you call function, you can check return status code, and if device is busy, wait some time and then send again.
 below is a code example, which get CD on position 10, and then Get it Down, assume deviceID is 100:

USBCDMoveto(  100, 10 ); // move to CD position 10 and Get it.

// Becase move CD to new position may spend some time, so we should wait until above command finished.
 // there are 2 method to do this:

Methos 1:

while ( USBCDGetCDDown( 100 ) !=  DEVICE_COMMANDOK  ) 
 {
 Sleep( 1000 ); // Pause 1 min
 }

Method 2:

while ( USBCDGetStatus( 100 ) !=  DEVICE_COMMANDOK  ) 
 {
 Sleep( 1000 ); // Pause 1 min
 }

USBCDGetCDDown( 100 );

6. Because USB is plug&play device, user may dynamic add or remove device, so you should monitor device change during application running and
     update and maintain your device list.
    Our library support a callback function, when device is add or remove, you callback function will called with now device number pass in.
    The callback function should be below type:

     typedef void (CALLBACK* CDDeviceChangePROC )(int);

   use SetCDCallbackProc( CDDeviceChangePROC lpProc ) to set your callback function.

  Ex :

    // Callback function
    void CALLBACK OnCDDeviceChange( int nDeviceNumber )
     {
       // Do what you want to do when device changed ( because user add or remove device )
     } 

  in your main code, use below code to set callback function:
      SetCDCallbackProc( (CDDeviceChangePROC)OnCDDeviceChange ); 

7.  Files needed for you to develop your own application to use our device locate on \Library forder. whic contain below files:
      sucdapi.dll : Our Dynamic link library.
      USBCDDLL.H, USBCDDLL.lib, USBCDDLL.exp : Files needed when use VC to import our library.
      DLLIMP.PAS : Delphi unit for yor to use sucdapi.dll

   We provide 2 samples for your reference:
      1. A VC++ sample to demo the usage of our library, this demo application is writter use Visual C++ 6.0.       
         Locate on \VCSample

     2. A Delphi sample to demo the usage of our library, this demo application is writter use Delphi 5.0.
         Locate on \D5Sample.
 </pre>

How they've documented it quite well by the sounds but I just don't know how to use their library in java.

There is a VC++ project sample too, but I don't really understand the code.  There is a USBCDLL.H file in that which is as follows:
 <pre>
 #ifndef _USBCDDLL_HPP
 #define _USBCDDLL_HPP

#define DEVICE_COMMANDOK   0
 #define DEVICE_IDERROR     1
 #define DEVICE_BUSY        2
 #define DEVICE_UNKNOWERROR  3

extern "C"
 {
     typedef void (CALLBACK* CDDeviceChangePROC )(int);
     
     WINUSERAPI VOID WINAPI InitUSBCDLibrary();
     WINUSERAPI VOID WINAPI CloseUSBCDLibrary();
     WINUSERAPI VOID WINAPI SetCDCallbackProc( CDDeviceChangePROC lpProc );
     WINUSERAPI int WINAPI GetDeviceNumber();
     WINUSERAPI int WINAPI EnumDevice( int);
     
     // CD Device Operate function below
     WINUSERAPI int WINAPI USBCDReset( int ID );
     WINUSERAPI int WINAPI USBCDMoveto( int ID, int Index );
     WINUSERAPI int WINAPI USBCDGetCDDown( int ID );    
     WINUSERAPI int WINAPI USBCDLEDON( int ID );
     WINUSERAPI int WINAPI USBCDLEDOFF( int ID ); 
     WINUSERAPI int WINAPI USBCDGetStatus( int ID ); 
 }

#endif

             */
        }
    }
}
